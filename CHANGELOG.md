# Changelog for "ecological-engine-smart-executor"


## [v1.6.8-SNAPSHOT] - 2022-12-06

- Updated bom to latest version [#24209]


## [v1.6.7] - 2022-04-05

- Fixed obsolete http link


## [v1.6.6] - 2021-10-06

- Fixed obsolete short urls [#20971]


## [v1.6.5] - 2021-01-20

- added commons-io-2.6 dependency
- update ecological-engine lower bound range


## [v1.6.4] - 2020-10-15

- Updated pom.xml for support gcube-bom-2.0.0-SNAPSHOT [#19790]


## [v1.6.3] - 2020-06-10

- Updated for support https protocol [#19423]


## [v1.6.0] - 2017-09-27

- Moved to the new SocialNetowrkingService 2.0


## [v1.4.0] - 2016-09-27

- Moved generic Worker to Dataminer


## [v1.3.0] - 2016-04-01

- Web application publisher


## [v1.2.0] - 2016-02-08

- Management of SAI algorithm


## [v1.0.0] - 2015-05-27

- First Release


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

